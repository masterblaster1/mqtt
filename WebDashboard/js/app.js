//Create a new Client object with your broker's hostname, port and your own clientId
var client = new Messaging.Client("broker.mqttdashboard.com", 8000, "dashboard");
var cputemp;
var usedmem;
var freemem;
$(document).ready(function () {

    cputemp = new JustGage({
        id: "cputemp",
        value: 0,
		min: 20,        
        max: 85,		
        title: " ",
		showMinMax: true,
        levelColors: ["#007CFF", "#a9d70b", "#ff0000"],
        levelColorsGradient: false,
        label: "° Celsius"
    })
	usedmem = new JustGage({
        id: "usedmem",
        value: 0,
        min: 0,
        max: 434,
        title: " ",
		showMinMax: true,
        levelColors: ["#007CFF", "#a9d70b", "#ff0000"],
        levelColorsGradient: false,
        label: "MB"
    })
	freemem = new JustGage({
        id: "freemem",
        value: 0,
		//min: "",
        //max: "",        
        title: " ",
		showMinMax: false,
        levelColors: ["#ff0000", "#a9d70b", "#007CFF"],
        levelColorsGradient: false,
        label: "MB"
    });


    var options = {

        //connection attempt timeout in seconds
        timeout: 3,

        //Gets Called if the connection has successfully been established
        onSuccess: function () {
			
			client.subscribe("home/status");
			client.subscribe("home/memoryfree");
            client.subscribe("home/cputemperature");
			client.subscribe("home/memoryused");            
        },

        //Gets Called if the connection could not be established
        onFailure: function (message) {
            alert("Connection failed: " + message.errorMessage);
        }
    };

    //Gets called whenever you receive a message for your subscriptions
    client.onMessageArrived = function (message) {

        var topic = message.destinationName;

        if (topic === "home/status") {
            if (message.payloadString === "offline") {
                $("#client_disconnected").html('Raspberry Pi Publisher ist Offline').removeClass("hide").hide().fadeIn("slow");
                $("#client_connected").addClass("hide").hide();
            } else {
                $("#client_connected").html('Raspberry Pi Publisher ist Online').removeClass("hide").hide().fadeIn("slow");
                $("#client_disconnected").addClass("hide").hide();

            }
        }
		else if(topic === "home/cputemperature") {
			setCPU(message.payloadString);
		}
		else if (topic === "home/memoryused") {
			setMEMU(message.payloadString);
		}
		else if (topic === "home/memoryfree") {
			setMEMF(message.payloadString);
		}
    };

	//Attempt to connect
    client.connect(options);
});

function setMEMF(temp1) {
	freemem.refresh(parseFloat(temp1));
}
function setCPU(temp2) {
	cputemp.refresh(parseFloat(temp2));
}
function setMEMU(temp3) {
	usedmem.refresh(parseFloat(temp3));
}