package publisher;

//Paho-Bibliotheken + Utils package
import util.Utils;
import org.eclipse.paho.client.mqttv3.*;

//pi4j-Bibliotheken (ermoeglichen das Auslesen von HW-Informationen des RaspPI)
import com.pi4j.system.SystemInfo;
import java.io.IOException;

public class Publisher {
	
	//ObjektVariablen
	public static final String BROKER_URL = "tcp://broker.mqttdashboard.com:1883";
	private MqttClient client;
	
	public static final String TOPIC_CPU_TEMPERATURE = "home/cputemperature";
	public static final String TOPIC_MEMORY_USED = "home/memoryused";
	public static final String TOPIC_MEMORY_FREE = "home/memoryfree";
	public static final int mb = 1024 * 1024;
	
	//Konstruktor.
	public Publisher() {
		String clientId = Utils.getMacAddress() + "-pub";
		try {
			//Instanz der Klasse MqttClient erzeugen.
			client = new MqttClient(BROKER_URL, clientId);
		} catch(MqttException e) {
		  e.printStackTrace();
		  System.exit(1);
		}		
	}
	
	private void start() throws NumberFormatException, IOException {
		
		try {
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(false);
			options.setWill(client.getTopic("home/status"), "offline".getBytes(), 1, true);
			
			client.connect(options);
			
			client.publish("home/status","online".getBytes(),1,true);
			
			//Publish data
			while(true) {
				publishCpuTemperature();
				Thread.sleep(500);
				publishMemoryUsed();
				Thread.sleep(500);
				publishMemoryFree();
				Thread.sleep(500);
			}			
		} catch(MqttException e) {
		  e.printStackTrace();
		  System.exit(1);			
		} catch(InterruptedException e) {
		  e.printStackTrace();			
		}	
	}	
	
	//Publish-Funktion fuer die CPU-Temperatur
	private void publishCpuTemperature() throws MqttException, NumberFormatException, IOException, InterruptedException {
		
		final MqttTopic temperatureTopic = client.getTopic(TOPIC_CPU_TEMPERATURE);
		
		final float temperatureNumber = SystemInfo.getCpuTemperature();
		final String temperature = Float.toString(temperatureNumber);
		
		temperatureTopic.publish(temperature.getBytes(),1,true);
		
		System.out.println("Published data. Topic: " + temperatureTopic.getName() + " Message: " + temperature);		
	}
	
	//Publish-Funktion fuer Memory-Used
	private void publishMemoryUsed() throws MqttException, IOException, InterruptedException {
			
		final MqttTopic memoryTopic = client.getTopic(TOPIC_MEMORY_USED);
			
		final float memoryNumber = SystemInfo.getMemoryUsed() / mb;
		final String memoryused = Float.toString(memoryNumber);
			
		memoryTopic.publish(memoryused.getBytes(),1,true);
			
		System.out.println("Published data. Topic: " + memoryTopic.getName() + " Message: " + memoryused);
	}
		
	//Publish-Funktion fuer Memory-Free
	private void publishMemoryFree() throws MqttException, IOException, InterruptedException {
			
		final MqttTopic memoryTopic = client.getTopic(TOPIC_MEMORY_FREE);
			
		final float memoryNumber = SystemInfo.getMemoryFree() / mb;
		final String memoryfree = Float.toString(memoryNumber);
			
		memoryTopic.publish(memoryfree.getBytes(),1,true);
			
		System.out.println("Published data. Topic: " + memoryTopic.getName() + " Message: " + memoryfree);
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		final Publisher publisher = new Publisher();
		publisher.start();		
	}	
}